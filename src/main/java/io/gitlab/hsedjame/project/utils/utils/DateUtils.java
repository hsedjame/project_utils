package io.gitlab.hsedjame.project.utils.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.*;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @Project MYALDOC_PORJECT
 * @Author Henri Joel SEDJAME
 * @Date 12/02/2019
 * @Class purposes : .......
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateUtils {

    public static LocalDateTime mostRecent(LocalDateTime date1, LocalDateTime date2) {
        if (date1.isBefore(date2)) return date1;
        return date2;
    }

    public static LocalDateTime lessRecent(LocalDateTime date1, LocalDateTime date2) {
        if (date1.isBefore(date2)) return date2;
        return date1;
    }

    public static Optional<LocalDateTime> todayAt(LocalTime time) {
        return dayAt(LocalDateTime.now(), time);
    }

    public static Optional<LocalDateTime> dayAt(LocalDateTime day, LocalTime time) {
        if (Objects.isNull(day) || Objects.isNull(time)) return Optional.empty();
        return Optional.of(day.toLocalDate().atTime(time));
    }

    public static List<DayOfWeek> defaultWorkingDays() {
        return Arrays.asList(DayOfWeek.values()).subList(0, 5);
    }

    public static List<Month> monthsOfYearStartingByCurrentMonth() {
        return monthsOfYearStartingBy(LocalDate.now().getMonth());
    }

    public static List<Month> monthsOfYearStartingBy(Month month) {
        return Arrays.asList(Month.values())
                .subList(month.getValue() - 1, 12);
    }
}
