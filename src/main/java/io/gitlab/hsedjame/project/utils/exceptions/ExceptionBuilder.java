package io.gitlab.hsedjame.project.utils.exceptions;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public abstract class ExceptionBuilder<T extends Exception> {

    @Getter
    private List<T> exceptions = new ArrayList<>();
    @Getter
    private Class<T> exceptionClass;


    public ExceptionBuilder(Class<T> exceptionClass) {
        this.exceptionClass = exceptionClass;
    }

    /**
     * Méthode permettant d'ajouter une nouvelle exception au builder
     *
     * @param message le message d'erreur
     */
    public void addException(String message) {
        T exception = null;
        try {
            Constructor<T> constructor = this.exceptionClass.getConstructor(String.class);
            exception = constructor.newInstance(message);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            log.error(e.getMessage());
        }
        this.exceptions.add(exception);
    }

    /**
     * @param message   le message d'erreur
     * @param throwable l'exception d'origine
     */
    public void addException(String message, Throwable throwable) {
        T exception = null;
        try {
            Constructor<T> constructor = this.exceptionClass.getConstructor(String.class, Throwable.class);
            exception = constructor.newInstance(message, throwable);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            log.error(e.getMessage());
        }
        this.exceptions.add(exception);
    }

    /**
     * Méthode permettant de vider la liste des arguments du builder
     */
    public void clear() {
        log.info("ExceptionBuilder in cleaning...");
        this.exceptions.clear();
    }

    /**
     * Méthode permettant de construire une exception avec un message résumant les différentes erreurs de la liste des exceptions
     *
     * @return l'exception
     */
    public T buildException() {

        T exception = null;

        final String errorMessage = this.exceptions.stream()
                .map(e -> MessageFormat.format("{0} ( {1} )", e.getMessage(), e.getCause().getMessage()))
                .collect(Collectors.joining(MessageFormat.format("{0}", "\n")));

        try {
            Constructor<T> constructor = exceptionClass.getConstructor(String.class);
            exception = constructor.newInstance(errorMessage);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            log.error(e.getMessage());
        }

        this.clear();

        return exception;
    }

    /**
     * Méthode permettant de construire une exception avec un message résumant les différentes erreurs de la liste des exceptions
     *
     * @return l'exception
     */
    public T buildException(String message, Throwable t) {

        T exception = null;

        try {
            Constructor<T> constructor = exceptionClass.getConstructor(String.class, Throwable.class);
            exception = constructor.newInstance(message, t);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            log.error(e.getMessage());
        }

        this.clear();

        return exception;
    }

    /**
     * Méthode permettant de lancer l'exception finale
     *
     * @throws T l'exception levée
     */
    public boolean throwException() throws T {
        if (this.hasException()) throw this.buildException();
        return false;
    }

    /**
     * Méthode permettant de savoir si le builder a des exceptions
     *
     * @return vrai si le builder contient au moins une exception
     */
    public boolean hasException() {
        return !this.exceptions.isEmpty();
    }

    /**
     * Metode permettant de vérifier qu'il y aucune exception
     *
     * @return vrai si le builder est vide
     */
    public boolean isEmpty() {
        return this.exceptions.isEmpty();
    }
}
