package io.gitlab.hsedjame.project.utils.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * @Project MYALDOC
 * @Author Henri Joel SEDJAME
 * @Date 24/12/2018
 * @Class purposes : .......
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConcatUtils {

    public static String concat(String... strings) {
        return String.join("", strings);
    }
}
