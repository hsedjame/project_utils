package io.gitlab.hsedjame.project.utils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectUtilsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectUtilsApplication.class, args);
    }

}
