package io.gitlab.hsedjame.project.utils.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project MYALDOC_PORJECT
 * @Author Henri Joel SEDJAME
 * @Date 31/01/2019
 * @Class purposes : .......
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class LoggingUtils {

    public static void log(String message) {
        final int length = message.length();
        StringBuilder builder = new StringBuilder();
        String str1 = "************************************";
        String str2 = "**************";
        builder.append(str1);
        for (int i = 0; i < length; i++) {
            builder.append("*");
        }
        final String str3 = builder.toString();
        log.info(str3);
        log.info(str2 + "    " + message + "    " + str2);
        log.info(str3);
    }

    public static String toUpperCase(String source) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < source.length(); i++) {
            final char letter = source.charAt(i);
            if (Character.isUpperCase(source.charAt(i))) builder.append("_");
            builder.append(letter);
        }
        return builder.toString().toUpperCase();
    }
}
